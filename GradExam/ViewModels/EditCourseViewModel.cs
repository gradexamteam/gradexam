﻿using GradExam.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace GradExam.ViewModels
{
    public class EditCourseViewModel
    {
        public Course Course { get; set; }
        public string UserId { get; set; }
        public IEnumerable<User> Instructors { get; set; }
        public IEnumerable<Semester> Semesters { get; set; }
    }
}