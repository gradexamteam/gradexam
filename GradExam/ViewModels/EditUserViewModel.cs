﻿using GradExam.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.ViewModels
{
    public class EditUserViewModel
    {
        public User User { get; set; }
        public List<IdentityRole> UserCurrentRoles { get; set; }
        public List<SelectListItem> Roles { get; set; }
        public string SelectedRole { get; set; }

    }
}
