﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.ViewModels
{
    public class BaseViewModel
    {
        public bool ReadOnly { get; set; } = false;
    }
}
