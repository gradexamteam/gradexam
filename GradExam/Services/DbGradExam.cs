﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GradExam.Services
{
    public class DbGradExam : IGradeExamRepository
    {
        #region Variables
        private ApplicationDbContext _db;
        #endregion

        #region Constructors

        public DbGradExam(ApplicationDbContext db)
        {
            _db = db;
        }

        #endregion

        #region Roles
        public List<IdentityRole> GetListOfRoles()
        {
            return _db.Roles.ToList();
        }

        public IdentityRole GetRole(string id)
        {
            return _db.Roles.Where(r => r.Id == id).SingleOrDefault();
        }

        public List<IdentityRole> GetRolesOfUser(string userId)
        {
            var ret = new List<IdentityRole>();

            var rolesAssoc = _db.UserRoles.Where(ur => ur.UserId == userId).ToList();

            foreach(var n in rolesAssoc)
            {
                ret.Add(GetRole(n.RoleId));
            }

            return ret;
        }
        #endregion  

        #region User
        public User CreateUser(User user)
        {
            _db.User.Add(user);
            _db.SaveChanges();
            return user;
        }

        public void DeleteUser(int id)
        {
            var user = _db.User.Find(id);
            _db.User.Remove(user);
            _db.SaveChanges();
        }

        public User Read(string id)
        {
            return _db.User.Find(id);
        }

        public ICollection<User> ReadAllUsers()
        {
            return _db.User.ToList();
        }

        public void Update(string id, User user)
        {
            var oldPerson = Read(id);
            if (oldPerson != null)
            {
                oldPerson.FirstName = user.FirstName;
                oldPerson.LastName = user.LastName;
                oldPerson.Email = user.Email;
                _db.SaveChanges();
          }

        }
        #endregion

        #region Learning Outcome
        public LearnOutcome CreateLearningOutcome(LearnOutcome learnOutcome)
        {
            _db.LearnOutcome.Add(learnOutcome);
            _db.SaveChanges();
            return learnOutcome;
        }

        public void DeleteOutcome(int id)
        {
            LearnOutcome learnOutcome = _db.LearnOutcome.Find(id);
            _db.LearnOutcome.Remove(learnOutcome);
            _db.SaveChanges();
        }

        public LearnOutcome ReadOutcome(int id)
        {
            return _db.LearnOutcome.Include(l => l.QuestionAnswer).FirstOrDefault(c => c.Id == id);
        }

        public ICollection<LearnOutcome> ReadAllOutcomes()
        {
            return _db.LearnOutcome.Include(l => l.QuestionAnswer).ToList();
        }

        public void UpdateOutcome(int id, LearnOutcome learnOutcome)
        {
            var oldOutcome = ReadOutcome(id);
            if (oldOutcome != null)
            {
                oldOutcome.LearningOutcome = learnOutcome.LearningOutcome;
                oldOutcome.QuestionAnswer = learnOutcome.QuestionAnswer;
                _db.SaveChanges();
            }
        }

        #endregion

        #region Course

        public void DeleteCourse(int id)
        {
            Course course = _db.Courses.Find(id);
            _db.Courses.Remove(course);
            _db.SaveChanges();
        }

        public ICollection<Course> ReadAllCourses()
        {
            return _db.Courses.Include(u => u.Instructor).Include(c => c.Concentration).Include(c => c.Semester).Include(c => c.StudentCourses).ThenInclude(c => c.Student).ToList();
        }

        public Course ReadCourse(int id)
        {
            return _db.Courses.Include(u => u.Instructor).Include(c => c.Concentration).Include(c => c.LearnOutcomes).Include(c => c.Semester).Include(c => c.StudentCourses).ThenInclude(c => c.Student).Include(c => c.QuesAnswers).FirstOrDefault(c => c.Id == id);
        }

        public void UpdateCourse(int id, Course course)
        {
            var oldCourse = ReadCourse(id);
            if (oldCourse != null)
            {
                oldCourse.Name = course.Name;
                oldCourse.Section = course.Section;
                oldCourse.Semester = course.Semester;
                oldCourse.Instructor = course.Instructor;
                _db.SaveChanges();
            }

        }

        public Course AddCourse(Course Course)
        {
            _db.Courses.Add(Course);
            _db.SaveChanges();
            return Course;
        }


        #endregion

        #region QuesAnswer

        public QuesAnswer ReadQuesAnswer(int id)
        {
            return _db.QuesAnswer.Include(q => q.Course).ThenInclude(q => q.StudentCourses).ThenInclude(q => q.Student)
                .Include(q => q.LearnOutcome).FirstOrDefault(c => c.Id == id);
        }

        public ICollection<QuesAnswer> ReadAllQuesAnswers()
        {
            return _db.QuesAnswer.Include(q => q.Course).Include(q => q.LearnOutcome).ToList();
        }

        public QuesAnswer createQuesetion(QuesAnswer Qa)
        {
            _db.QuesAnswer.Add(Qa);
            _db.SaveChanges();
            return Qa;
        }

        public void deleteQuestion(int id)
        {
            var ques = _db.QuesAnswer.Find(id);
            _db.QuesAnswer.Remove(ques);
            _db.SaveChanges();
        }

        public void UpdateQuestion(int id, QuesAnswer ques)
        {
            var oldQuestion = ReadQuesAnswer(id);
            if (oldQuestion != null)
            {
                oldQuestion.Question = ques.Question;
                oldQuestion.Answer = ques.Answer;
                oldQuestion.Course = ques.Course;
                _db.SaveChanges();
            }
        }
        #endregion

        #region Exam
        public ICollection<Exam> ReadAllExams()
        {
            return _db.Exams.Include(c => c.Student).ThenInclude(c => c.StudentCourses).ThenInclude(c=> c.Course).ToList();
        }

        public Exam ReadExam(int id)
        {
            var exam = _db.Exams.Include(e => e.Student).ThenInclude(s => s.StudentCourses).ThenInclude(sc => sc.Course).ThenInclude(c => c.Instructor)
                                .Include(e => e.Student).ThenInclude(s => s.StudentCourses).ThenInclude(sc => sc.Course).ThenInclude(c => c.QuesAnswers).FirstOrDefault(e => e.Id == id);

            return exam;
        }

        public Exam CreateExam(int studentId)
        {
            var student = ReadStudent(studentId);
            var exam = new Exam
            {
                Student = student
            };

            // Add the part that if there is a student with an exam already going then it should just return that exam
            var testExam = _db.Exams.FirstOrDefault(e => e.Student.Id == studentId);

            if (testExam != null)
            {
                return testExam;
            }

            var ex = _db.Exams.Add(exam);
            _db.SaveChanges();

            foreach (var sc in student.StudentCourses)
            {
                foreach (var item in sc.Course.QuesAnswers)
                {
                    var qr = new QuestionResponse
                    {
                        Course = sc.Course,
                        Exam = ex.Entity,
                        Question = item,
                    };

                    _db.Responses.Add(qr);
                }
            }
            _db.SaveChanges();

            return ReadExam(ex.Entity.Id);
        }


        #endregion

        #region Concentration
        public ICollection<Concentration> ReadAllConcentrations()
        {
            return _db.Concentrations.ToList();
        }



        public Concentration ReadConcentration(int id)
        {
            return _db.Concentrations.FirstOrDefault(c => c.Id == id);
        }

        public Concentration UpdateConcentration(int id, Concentration c)
        {
            if (id == -1)
            {
                var conc = _db.Concentrations.Add(c);
                _db.SaveChanges();
                return c;
            }

            var concentration = _db.Concentrations.FirstOrDefault(con => con.Id == c.Id);
            concentration.Name = c.Name;

            _db.SaveChanges();

            return c;
        }

        public void DeleteConcentration(int id)
        {
            var con = _db.Concentrations.Find(id);
            _db.Concentrations.Remove(con);
            _db.SaveChanges();
        }

        #endregion

        #region Student

        public ICollection<Student> ReadAllStudents()
        {
            return _db.Student.Include(c => c.StudentCourses).ThenInclude(c => c.Course).ToList();
        }

        public Student ReadStudent(int id)
        {
            return _db.Student.Include(c => c.StudentCourses).ThenInclude(s => s.Course).ThenInclude(c => c.Instructor)
                              .Include(c => c.StudentCourses).ThenInclude(s => s.Course).ThenInclude(c => c.QuesAnswers).FirstOrDefault(c => c.Id == id);
        }

        public void UpdateStudent(int id, Student student)
        {
            var oldStudent = ReadStudent(id);
            if (oldStudent != null)
            {
                oldStudent.Name = student.Name;

                _db.SaveChanges();
            }

        }

        public Student AddStudent(Student student)
        {
            _db.Student.Add(student);
            _db.SaveChanges();
            return student;
        }

        public void DeleteStudent(int id)
        {
            Student student = _db.Student.Find(id);
            _db.Student.Remove(student);
            _db.SaveChanges();
        }

        #endregion

        #region Semester
        public List<Semester> ReadAllSemesters()
        {
            return _db.Semester.ToList();
        }

        public Semester ReadSemester(int id)
        {
            return _db.Semester.SingleOrDefault(s => s.SemesterId == id);
        }

        public Semester Create(Semester semester)
        {
            _db.Semester.Add(semester);
            _db.SaveChanges();
            return semester;
        }

        public bool UpdateSemester(int id, Semester semester)
        {
            var oldSemester = ReadSemester(id);
            var sameSemesters = ReadAllSemesters().Where(sm => sm.Season == semester.Season && sm.Year == semester.Year && sm.SemesterId != semester.SemesterId).Count();

            if(semester != null)
            {
                // if there's not a semester with the same properties then you can save it
                if(sameSemesters == 0)
                {
                    oldSemester.Season = semester.Season;
                    oldSemester.Year = semester.Year;
                    _db.SaveChanges();

                    return true;
                }
            }

            return false;
        }

        public bool DeleteSemester(int id)
        {
            try
            {
                Semester semester = _db.Semester.Find(id);
                _db.Semester.Remove(semester);
                _db.SaveChanges();
                return true;
            }
            catch(SqlException ex)
            {
                return false;
            }
        }
        #endregion

        #region Question Response

        public void SaveResponse(QuestionResponse qr)
        {
            if (qr.Id == 0)
                _db.Responses.Add(qr);
            else
                _db.Responses.Update(qr);

            _db.SaveChanges();
        }

        public List<QuestionResponse> ReadAllResponses()
        {
            var items = _db.Responses.Include(qr => qr.Question).Include(ex => ex.Exam).Include(q => q.Question).ToList();
            return items;
        }

        public List<QuestionResponse> ReadResponsesByCourse(int courseId, int examId)
        {
            var response = _db.Responses.Include(c => c.Course).Include(qr => qr.Question).Include(ex => ex.Exam).Where(m => m.Course.Id == courseId && m.Exam.Id == examId).ToList();
            return response;
        }

        public QuestionResponse ReadResponse(int id)
        {
            return _db.Responses.SingleOrDefault(m => m.Id == id);
        }

        #endregion

        #region Association Stuff

        public void AddStudentToACourse(Student student, Course course)
        {
            StudentCourse relation = new StudentCourse
            {
                StudentId = student.Id,
                Student = student,
                CourseId = course.Id,
                Course = course
            };

            Course newCourse = ReadCourse(course.Id);
            newCourse.StudentCourses.Add(relation);

            _db.Courses.Update(newCourse);
            _db.SaveChanges();
        }

        public void RemoveStudentFromACourse(Student student, Course course)
        {

            Course newCourse = ReadCourse(course.Id);
            var relation = newCourse.StudentCourses.Where(s => s.StudentId == student.Id).SingleOrDefault();
            newCourse.StudentCourses.Remove(relation);

            _db.Courses.Update(newCourse);
            _db.SaveChanges();
        }

        public void AddLOToACourse(LearnOutcome lo, Course course)
        {
            Course newCourse = ReadCourse(course.Id);
            newCourse.LearnOutcomes.Add(lo);

            _db.Courses.Update(newCourse);
            _db.SaveChanges();
        }

        public void AddQAToACourse(QuesAnswer qa, Course course)
        {
            Course newCourse = ReadCourse(course.Id);
            newCourse.QuesAnswers.Add(qa);

            _db.Courses.Update(newCourse);
            _db.SaveChanges();
        }

        public void RemoveLOFromACourse(LearnOutcome lo, Course course)
        {
            Course newCourse = ReadCourse(course.Id);
            LearnOutcome courseLO = newCourse.LearnOutcomes.SingleOrDefault(m => m.Id == lo.Id);
            newCourse.LearnOutcomes.Remove(courseLO);

            _db.Courses.Update(newCourse);
            _db.SaveChanges();
        }

        public void RemoveQAFromACourse(QuesAnswer qa, Course course)
        {
            Course newCourse = ReadCourse(course.Id);
            QuesAnswer QA = newCourse.QuesAnswers.SingleOrDefault(m => m.Id == qa.Id);
            newCourse.QuesAnswers.Remove(QA);

            _db.Courses.Update(newCourse);
            _db.SaveChanges();
        }

        #endregion

        #region Helper/Reporting Methods

        public List<Tuple<string, double>> GetAverageByClass(int studentId)
        {
            List<Tuple<string, double>> ret = new List<Tuple<string, double>>();
            var responses = _db.Responses.Where(r => r.Exam.Student.Id == studentId).ToList();//.GroupBy(r => r.Course);
            var courses = _db.Courses.Where(s => s.StudentCourses.Where(sc => sc.StudentId == studentId).Any()).ToList();
            double avg;

            foreach (var c in courses)
            {
                if (responses.Where(r => r.Course.Id == c.Id).Any())
                    avg = responses.Where(r => r.Course.Id == c.Id).Average(res => res.Rating);
                else
                    avg = double.NaN;
                ret.Add(new Tuple<string, double>(c.Name, avg));
            }
            return ret;
        }

        public double GetAverageForCourse(int id)
        {
            double sum = 0;
            var responses = _db.Responses.Include(c => c.Course).Where(w => w.Course.Id == id);

            foreach (var n in responses)
            {
                sum += n.Rating;
            }
            if (sum != 0)
            {
                return sum / responses.Count();
            }
            else
            {
                return 0;
            }
        }

        public double GetAvgForConc(int id)
        {
            double sum = 0;
            var responses = _db.Responses.Include(c => c.Course).Where(w => w.Course.Concentration.Id == id);
            foreach (var n in responses)
            {
                sum += n.Rating;
            }
            if (sum != 0)
            {
                return sum / responses.Count();
            }
            else
            {
                return 0;
            }

        }

        public List<Report> ReadResponsesByAggregate(string aggregate, string time)
        {
            List<Report> responseArray = new List<Report>();
            if (aggregate == "Concentration")
            {
                ICollection<Concentration> concentrations = ReadAllConcentrations();
                foreach (var concentration in concentrations)
                {
                    List<Tuple<Course, double>> finalList = new List<Tuple<Course, double>>();
                    var courses = _db.Courses.Include(x => x.StudentCourses).ThenInclude(s => s.Student).Where(w => w.Concentration.Id == concentration.Id && w.Semester.ToString() == time).ToList();
                    foreach (var course in courses)
                    {
                        finalList.Add(new Tuple<Course, double>(course, GetAverageForCourse(course.Id)));
                    }
                    Report rep = new Report(concentration.Name, finalList);
                    rep.ConcentrationAvg = GetAvgForConc(concentration.Id);
                    responseArray.Add(rep);
                }
            }
            else if (aggregate == "Course")
            {
                var courses2 = _db.Courses.Include(x => x.StudentCourses).ThenInclude(s => s.Student).Where(w => w.Semester.ToString() == time).ToList();
                List<Tuple<Course, double>> finalList = new List<Tuple<Course, double>>();
                foreach (var course in courses2)
                {
                    finalList.Add(new Tuple<Course, double>(course, GetAverageForCourse(course.Id)));
                }
                Report rep = new Report(finalList);
                responseArray.Add(rep);
            }

            return responseArray;
        }

        public List<string> GetDistinctSemesters()
        {
            List<String> temp = new List<string>();
            List<Course> dirtySemesters = ReadAllCourses().ToList();
            foreach (var semester in dirtySemesters)
            {
                temp.Add(semester.Semester.ToString());
            }
            var distinctStrings = temp.Distinct().ToList();
            return distinctStrings;
        }

       

        #endregion

    }
}
