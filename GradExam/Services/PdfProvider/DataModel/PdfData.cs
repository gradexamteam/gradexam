﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace GradExam.Services.PdfProvider.DataModel
{
    public class PdfData
    {
        public string DocumentTitle { get; set; }
        public string CreatedBy { get; set; }
        public string ReportName { get; set; }
        public string ReportDescription { get; set; }
        public PdfTable Table { get; set; }
        public string DocumentName { get; set; }
        public List<string> ListToDisplay { get; set; }

        public PdfData(List<Object> list = null, string[] properties = null, string docTitle = "", string repName = "Default Report Name", string description = "", string user = "Unknown")
        {
            this.DocumentTitle = docTitle;
            this.CreatedBy = user;
            this.ReportName = repName;
            this.ReportDescription = description;

            if(list != null)
                this.Table = GetPdfTable(list, properties);

        }

        public PdfTable GetPdfTable(List<Object> list, string[] properties)
        {
            Type type;
            foreach(var n in list)
            {
                type = n.GetType();
                IList<PropertyInfo> props = new List<PropertyInfo>(type.GetProperties());

                foreach(PropertyInfo prop in props)
                {
                    object propValue;
                    if(properties.Contains(prop.Name))
                        propValue = prop.GetValue(n, null);

                    //Add row and columns
                }
            }

            return new PdfTable();
        }
    }
}
