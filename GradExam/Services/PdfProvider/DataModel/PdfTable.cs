﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Services.PdfProvider.DataModel
{
    public class PdfTable
    {
        public List<Tuple<string,int>> Headers { get; set; }
        public List<List<string>> RowData { get; set; }
    }
}
