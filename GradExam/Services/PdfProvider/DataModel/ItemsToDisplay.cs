﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Services.PdfProvider.DataModel
{
    public class ItemsToDisplay
    {
        public string Id { get; set; }

        public string Data1 { get; set; }

        public string Data2 { get; set; }
    }
}
