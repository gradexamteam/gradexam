﻿using GradExam.Services.PdfProvider.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Services.PdfProvider
{
    public interface IMigraDocService
    {
        string CreateMigraDocPdf(PdfData pdfData);
    }
}
