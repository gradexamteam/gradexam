﻿using GradExam.Services.PdfProvider.DataModel;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Fonts;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace GradExam.Services.PdfProvider
{
    public class PdfSharpService : IPdfSharpService
    {
        private string _createdDocsPath = ".\\PdfProvider\\Created";
        private string _imagesPath = ".\\PdfProvider\\Images";
        private string _resourcesPath = ".\\PdfProvider\\Resources";
        private string font = "OpenSans";
        private double nextLine = 100;

        public string CreatePdf(PdfData pdfData)
        {
            //if (GlobalFontSettings.FontResolver == null)
            //{
            //    GlobalFontSettings.FontResolver = new FontResolver(_resourcesPath);
            //}

            var document = new PdfDocument();
            var page = document.AddPage();
            var gfx = XGraphics.FromPdfPage(page);

            //AddTitleLogo(gfx, page, $"{_imagesPath}\\logo.jpg", 0, 0);
            if(pdfData.DocumentTitle != null)
                AddTitleAndFooter(page, gfx, pdfData.DocumentTitle, document, pdfData);

            if (pdfData.DocumentName != null)
                AddName(gfx, pdfData);

            if(pdfData.ReportDescription != null)
                AddDescription(gfx, pdfData);

            if(pdfData.Table != null)
                AddTable(gfx, pdfData.Table);

            if (pdfData.ListToDisplay != null)
                AddList(gfx, pdfData);

            string docName = $"{pdfData.DocumentName}-{DateTime.UtcNow.ToOADate()}.pdf";
            document.Save(docName);
            return docName;
        }

        private void AddName(XGraphics gfx, PdfData pdfData)
        {
            var font = new XFont(this.font, 14, XFontStyle.Regular);
            XTextFormatter tf = new XTextFormatter(gfx);
            XRect rect = new XRect(40, 100, 520, 30);
            gfx.DrawRectangle(XBrushes.White, rect);
            tf.DrawString($"Report: " + pdfData.ReportName, font, XBrushes.Black, rect, XStringFormats.TopLeft);
        }

        void AddTitleLogo(XGraphics gfx, PdfPage page, string imagePath, int xPosition, int yPosition)
        {
            if (!File.Exists(imagePath))
            {
                throw new FileNotFoundException(String.Format("Could not find image {0}.", imagePath));
            }

            XImage xImage = XImage.FromFile(imagePath);
            gfx.DrawImage(xImage, xPosition, yPosition, xImage.PixelWidth / 8, xImage.PixelHeight / 8);
        }

        void AddTitleAndFooter(PdfPage page, XGraphics gfx, string title, PdfDocument document, PdfData pdfData)
        {
            XRect rect = new XRect(new XPoint(), gfx.PageSize);
            rect.Inflate(-10, -15);
            XFont font = new XFont(this.font, 14, XFontStyle.Bold);
            gfx.DrawString(title, font, XBrushes.MidnightBlue, rect, XStringFormats.TopCenter);

            rect.Offset(0, 5);
            font = new XFont(this.font, 8, XFontStyle.Italic);
            XStringFormat format = new XStringFormat();
            format.Alignment = XStringAlignment.Near;
            format.LineAlignment = XLineAlignment.Far;
            gfx.DrawString("Created by " + pdfData.CreatedBy, font, XBrushes.DarkOrchid, rect, format);

            font = new XFont(this.font, 8);
            format.Alignment = XStringAlignment.Center;
            gfx.DrawString(document.PageCount.ToString(), font, XBrushes.DarkOrchid, rect, format);

            document.Outlines.Add(title, page, true);
        }

        void AddDescription(XGraphics gfx, PdfData pdfData)
        {
            var font = new XFont(this.font, 14, XFontStyle.Regular);
            XTextFormatter tf = new XTextFormatter(gfx);

            int amtNewLines = pdfData.ReportDescription.Split("\n").Count();

            XRect rect = new XRect(40, nextLine, 520, 100 + 20 * amtNewLines);
            nextLine = nextLine + rect.Height;
            gfx.DrawRectangle(XBrushes.White, rect);
            tf.DrawString(pdfData.ReportDescription, font, XBrushes.Black, rect, XStringFormats.TopLeft);
        }

        void AddList(XGraphics gfx, PdfData pdfData)
        {
            int startingHeight = 200;
            int listItemHeight = 30;

            for (int i = 0; i < pdfData.ListToDisplay.Count; i++)
            {
                if(pdfData.ListToDisplay[i].Contains("\n"))
                {
                    listItemHeight = listItemHeight * pdfData.ListToDisplay[i].Split("\n").Count();
                }

                var font = new XFont(this.font, 14, XFontStyle.Regular);
                XTextFormatter tf = new XTextFormatter(gfx);
                XRect rect = new XRect(60, startingHeight, 500, listItemHeight);
                gfx.DrawRectangle(XBrushes.White, rect);
                var data = $"{pdfData.ListToDisplay[i]}";
                tf.DrawString(data, font, XBrushes.Black, rect, XStringFormats.TopLeft);

                startingHeight = startingHeight + listItemHeight;
                listItemHeight = 30;    //reset line height
            }
        }

        void AddTable(XGraphics gfx, PdfTable table)
        {
            var font = new XFont(this.font, 14, XFontStyle.Regular);
            XTextFormatter tf = new XTextFormatter(gfx);

            var rowHeight = 20;
            var tableWidth = 580;
            var tableHeight = (table.RowData.Count + 1) * rowHeight;
            var columnWidth = tableWidth / table.Headers.Count;

            XPen pen = new XPen(XColors.Black, 2);
            nextLine += 50;

            // Header Information
            int rowPosition = 0;
            int position = 0;
            int widthSoFar = 30;
            foreach (var h in table.Headers)
            {
                int w = h.Item2 != 0 ? h.Item2 : columnWidth;
                XRect headColRect = new XRect(widthSoFar, nextLine, widthSoFar + w > tableWidth ? tableWidth - widthSoFar : w, rowHeight);
                gfx.DrawRectangle(pen, headColRect);
                widthSoFar = widthSoFar + w > tableWidth ? tableWidth : widthSoFar + w;

                position++;
                tf.DrawString(" " + h.Item1, font, XBrushes.Black, headColRect, XStringFormats.TopLeft);
            }

            // Column Information
            position = 0;
            rowPosition = 1;
            foreach (var row in table.RowData)
            {
                widthSoFar = 30;        // restart width
                foreach (var col in row)
                {
                    int w = table.Headers[position].Item2 != 0 ? table.Headers[position].Item2 : columnWidth;
                    XRect rowColRect = new XRect(widthSoFar, nextLine + 20 * rowPosition, widthSoFar + w > tableWidth ? tableWidth - widthSoFar : w, rowHeight);
                    gfx.DrawRectangle(pen, rowColRect);
                    widthSoFar = widthSoFar + w > tableWidth ? tableWidth : widthSoFar + w;
                    position++;

                    tf.DrawString(" " + col, font, XBrushes.Black, rowColRect, XStringFormats.TopLeft);
                }
                position = 0;
                rowPosition++;
            }
        }
    }
}
