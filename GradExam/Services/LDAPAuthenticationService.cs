﻿using Novell.Directory.Ldap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Services
{
    public class LDAPAuthenticationService
    {
        //TODO: might make all of this static

        private static ILdapConnection _conn;
        public ILdapConnection GetConnection()
        {

            // Creating an LdapConnection instance 
            using (var ldapConn = new LdapConnection())
            {
                //Connect function will create a socket connection to the server - Port 389 for insecure and 3269 for secure    
                ldapConn.Connect("ldap.etsu.edu", 389);

                //Bind function with null user dn and password value will perform anonymous bind to LDAP server 
                ldapConn.Bind(@"username", "password");

            }



            return new LdapConnection();
        }

        //HashSet<string> SearchForGroup(string groupName)
        //{
        //    var ldapConn = GetConnection();
        //    var groups = new HashSet<string>();

        //    var searchBase = string.Empty;
        //    var filter = $"(&(objectClass=group)(cn={groupName}))";
        //    var search = ldapConn.Search(searchBase, LdapConnection.SCOPE_SUB, filter, null, false);
        //    while (search.hasMore())
        //    {
        //        var nextEntry = search.next();
        //        groups.Add(nextEntry.DN);
        //        var childGroups = GetChildren(string.Empty, nextEntry.DN);
        //        foreach (var child in childGroups)
        //        {
        //            groups.Add(child);
        //        }
        //    }

        //    return groups;
        //}

        //static HashSet<string> GetChildren(string searchBase, string groupDn, string objectClass = "group")
        //{
        //    var ldapConn = GetConnection();
        //    var listNames = new HashSet<string>();

        //    var filter = $"(&(objectClass={objectClass})(memberOf={groupDn}))";
        //    var search = ldapConn.Search(searchBase, LdapConnection.SCOPE_SUB, filter, null, false);

        //    while (search.hasMore())
        //    {
        //        var nextEntry = search.next();
        //        listNames.Add(nextEntry.DN);
        //        var children = GetChildren(string.Empty, nextEntry.DN);
        //        foreach (var child in children)
        //        {
        //            listNames.Add(child);
        //        }
        //    }

        //    return listNames;
        //}

        //void SearchForUser(string company, HashSet<string> groups = null)
        //{
        //    var ldapConn = GetConnection();
        //    var users = new HashSet<string>();

        //    string groupFilter = (groups?.Count ?? 0) > 0 ?
        //        $"(|{string.Join("", groups.Select(x => $"(memberOf={x})").ToList())})" :
        //        string.Empty;
        //    var searchBase = string.Empty;
        //    string filter = $"(&(objectClass=user)(objectCategory=person)(company={company}){groupFilter})";
        //    var search = ldapConn.Search(searchBase, LdapConnection.SCOPE_SUB, filter, null, false);

        //    while (search.hasMore())
        //    {
        //        var nextEntry = search.next();
        //        nextEntry.getAttributeSet();
        //        users.Add(nextEntry.DN);
        //    }
        //}

    }
}
