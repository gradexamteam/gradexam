﻿using GradExam.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Services
{
    public interface IGradeExamRepository
    {
        #region User
        User CreateUser(User user);
        User Read(string id);
        ICollection<User> ReadAllUsers();
        void Update(string id, User user);
        #endregion

        #region Roles
        List<IdentityRole> GetListOfRoles();
        IdentityRole GetRole(string id);
        List<IdentityRole> GetRolesOfUser(string userId);

        #endregion

        #region Course
        Course ReadCourse(int id);
        ICollection<Course> ReadAllCourses();
        void DeleteCourse(int id);
        void UpdateCourse(int id, Course course);
        Course AddCourse(Course Course);
        void AddLOToACourse(LearnOutcome lo, Course course);
        void RemoveLOFromACourse(LearnOutcome lo, Course course);
        List<string> GetDistinctSemesters();
        #endregion

        #region Question and Answers
        QuesAnswer ReadQuesAnswer(int id);
        ICollection<QuesAnswer> ReadAllQuesAnswers();
        void UpdateQuestion(int id, QuesAnswer ques);
        void deleteQuestion(int id);
        QuesAnswer createQuesetion(QuesAnswer QA);
        void AddQAToACourse(QuesAnswer qa, Course course);
        void RemoveQAFromACourse(QuesAnswer qa, Course course);
        #endregion

        #region Concentration
        ICollection<Concentration> ReadAllConcentrations();
        Concentration ReadConcentration(int id);
        Concentration UpdateConcentration(int id, Concentration c);
        void DeleteConcentration(int id);
        #endregion

        #region LearningOutcome
        LearnOutcome CreateLearningOutcome(LearnOutcome learnOutcome);
        void UpdateOutcome(int id, LearnOutcome learnOutcome);
        ICollection<LearnOutcome> ReadAllOutcomes();
        LearnOutcome ReadOutcome(int id);
        void DeleteOutcome(int id);
        #endregion

        #region Student
        Student ReadStudent(int id);
        ICollection<Student> ReadAllStudents();
        void DeleteStudent(int id);
        void UpdateStudent(int id, Student student);
        Student AddStudent(Student Student);
        List<Tuple<string, double>> GetAverageByClass(int studentId);
        #endregion

        #region StudentCourses
        void AddStudentToACourse(Student student, Course course);
        void RemoveStudentFromACourse(Student student, Course course);
        #endregion

        #region Exam
        Exam ReadExam(int id);
        Exam CreateExam(int studentId);

        void SaveResponse(QuestionResponse qr);
        List<QuestionResponse> ReadAllResponses();

        double GetAverageForCourse(int id);
        double GetAvgForConc(int id);
        List<QuestionResponse> ReadResponsesByCourse(int courseId, int examId);
        List<Report> ReadResponsesByAggregate(string aggregate, string time);
        QuestionResponse ReadResponse(int id);
        ICollection<Exam> ReadAllExams();
        #endregion

        #region Semester 
        List<Semester> ReadAllSemesters();
        Semester ReadSemester(int id);
        Semester Create(Semester semester);
        bool UpdateSemester(int id, Semester semester);
        bool DeleteSemester(int id);
        #endregion

    }
}
