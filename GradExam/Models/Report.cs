﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Models
{
    public class Report {
        public string Name;
        public List<Tuple<Course, double>> Courses;
        public double ConcentrationAvg { set; get; }

        public Report(string name, List<Tuple<Course, double>> courses) {
            Name = name;
            Courses = courses;
        }

        public Report(List<Tuple<Course, double>> courses) {
            Courses = courses;
        }
    }
    

}
