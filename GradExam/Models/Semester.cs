﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Models
{
    public class Semester
    {
        [Key]
        public int SemesterId { get; set; }
        public Season Season { get; set; }
        public int Year { get; set; }
        public string DisplayName { get { return Season.ToString() + " " + Year; } }

        public override string ToString()
        {
            return Season.ToString() + " " + Year;
        }

    }

    public enum Season
    {
        Spring = 1,
        Summer = 2,
        Fall = 3,
        Winter = 4
    }
}