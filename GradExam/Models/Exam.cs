﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Models
{
    public class Exam
    {
        public int Id { get; set; }
        public Student Student { get; set; }
    }
}
