﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using Microsoft.AspNetCore.Authorization;
using GradExam.ViewModels;
using Microsoft.AspNetCore.Mvc;
using GradExam.Services.PdfProvider;
using GradExam.Services.PdfProvider.DataModel;
using System.IO;

namespace GradExam.Controllers
{
    [Authorize(Roles = "Admin,Director")]
    public class CourseController : Controller
    {
        private IGradeExamRepository context;
        private IMigraDocService migraDocService;

        public CourseController(IGradeExamRepository context, IMigraDocService migraDocService)
        {
            this.context = context;
            this.migraDocService = migraDocService;
        }

        public IActionResult Delete(int id)
        {
            var course = context.ReadCourse(id);
            if (course == null)
            {
                return RedirectToAction("Index");
            }
            return View(course);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            context.DeleteCourse(id);
            return RedirectToAction("Index");
        }

         public IActionResult Details(int id)
        {
            var listAllStudents = context.ReadAllStudents().ToList();
            var listAllOutcomes = context.ReadAllOutcomes().ToList();
            var listAllQuesAnswers = context.ReadAllQuesAnswers().ToList();

            var listStudents = new List<Student>();
            var listLearnOutcomes = new List<LearnOutcome>();
            var listQuesAnswers = new List<QuesAnswer>();

            var studentsInCourse = new List<Student>();
            foreach(var item in context.ReadCourse(id).StudentCourses)
            {
                studentsInCourse.Add(item.Student);
            }

            foreach(var student in listAllStudents)
            {
                if(!studentsInCourse.Contains(student))
                {
                    listStudents.Add(student);
                }
            }

            var learnOutcomeForCourse = new List<LearnOutcome>();
            foreach (var item in context.ReadCourse(id).LearnOutcomes)
            {
                learnOutcomeForCourse.Add(item);
            }

            foreach (var lo in listAllOutcomes)
            {
                if (!learnOutcomeForCourse.Contains(lo))
                {
                    listLearnOutcomes.Add(lo);
                }
            }

            var quesAnswerForCourse = new List<QuesAnswer>();
            foreach (var item in context.ReadCourse(id).QuesAnswers)
            {
                quesAnswerForCourse.Add(item);
            }

            foreach (var qa in listAllQuesAnswers)
            {
                if (!quesAnswerForCourse.Contains(qa))
                {
                    listQuesAnswers.Add(qa);
                }
            }

            ViewBag.Students = listStudents;
            ViewBag.LearningOutcomes = listLearnOutcomes;
            ViewBag.QuesAnswers = listQuesAnswers;

            var course = context.ReadCourse(id);
            if (course == null)
            {
                return RedirectToAction("Index");
            }
            return View(course);
        }

        [Authorize(Roles = "Admin, Instructor")]
        public IActionResult Add()
        {
            // Get the list of Users
            var listInstructors = context.ReadAllUsers().ToList();

            // Get the list of Semesters
            var listSemesters = context.ReadAllSemesters().ToList();

            var vm = new EditCourseViewModel()
            {
                Instructors = listInstructors,
                Semesters = listSemesters
            };

            return View(vm);
        }
        [HttpPost]
        public IActionResult Add(EditCourseViewModel vm)
        {

            if (ModelState.IsValid)
            {
                vm.Course.Instructor = context.Read(vm.Course.Instructor.Id);
                vm.Course.Semester = context.ReadSemester(vm.Course.Semester.SemesterId);
                context.AddCourse(vm.Course);
                return RedirectToAction("Index");
            }

            // Get the list of Users
            vm.Instructors = context.ReadAllUsers().ToList();

            // Get the list of Semesters
            vm.Semesters = context.ReadAllSemesters().ToList();

            return View(vm);

        }

		public IActionResult Index()
		{
            IEnumerable<Course> courses = context.ReadAllCourses();

			return View(courses);
		}

        public IActionResult Edit(int id)
        {

            // Get the list of Users
            var listInstructors = new List<User>();
            listInstructors = context.ReadAllUsers().ToList();

            ViewBag.Instructors = listInstructors;

            // Get the list of Semesters
            var listSemesters = new List<Semester>();
            listSemesters = context.ReadAllSemesters().ToList();

            ViewBag.Semesters = listSemesters;

            var course = context.ReadCourse(id);
            if (course == null)
            {
                return RedirectToAction("Index");
            }

            var vm = new EditCourseViewModel()
            {
                Course = course,
                UserId = course.Instructor.Id,
            };

            return View(vm);
        }

        [HttpPost]
        public IActionResult Edit(EditCourseViewModel vm)
        {
            if (ModelState.IsValid)
            {
                vm.Course.Instructor = context.Read(vm.UserId);
                vm.Course.Semester = context.ReadSemester(vm.Course.Semester.SemesterId);
                context.UpdateCourse(vm.Course.Id, vm.Course);
                return RedirectToAction("Index");
            }
            return View(vm);
        }

        public IActionResult AddStudent(int studentId, int courseId)
        {
            try
            {
                if (!ModelState.IsValid || studentId == 0)
                {
                    return RedirectToAction("Details", new { id = courseId });
                }

                var student = context.ReadStudent(studentId);
                var course = context.ReadCourse(courseId);
                context.AddStudentToACourse(student, course);
            }
            catch(Exception ex)
            {
                return Content(ex.ToString());
            }

            return RedirectToAction("Details", new { id = courseId });
        }

        public IActionResult RemoveStudent(int studentId, int courseId)
        {
            try
            {
                var student = context.ReadStudent(studentId);
                var course = context.ReadCourse(courseId);
                context.RemoveStudentFromACourse(student, course);
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }

            return RedirectToAction("Details", new { id = courseId });
        }

        public IActionResult AddLearningOutcome(int learningOutcomeId, int courseId)
        {
            try
            {
                if(!ModelState.IsValid || learningOutcomeId == 0)
                {
                    return RedirectToAction("Details", new { id = courseId });
                }

                var lo = context.ReadOutcome(learningOutcomeId);
                var course = context.ReadCourse(courseId);
                context.AddLOToACourse(lo, course);
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }

            return RedirectToAction("Details", new { id = courseId });
        }

        public IActionResult RemoveLearningOutcome(int learningOutcomeId, int courseId)
        {
            try
            {
                var lo = context.ReadOutcome(learningOutcomeId);
                var course = context.ReadCourse(courseId);
                context.RemoveLOFromACourse(lo, course);
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }

            return RedirectToAction("Details", new { id = courseId });
        }
		public IActionResult AddQuestionAnswer(int questionAnswerId, int courseId)
        {
            try
            {
                if(!ModelState.IsValid || questionAnswerId == 0)
                {
                    return RedirectToAction("Details", new { id = courseId });
                }

                var qa = context.ReadQuesAnswer(questionAnswerId);
                var course = context.ReadCourse(courseId);
                context.AddQAToACourse(qa, course);
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }

            return RedirectToAction("Details", new { id = courseId });
        }
		public IActionResult RemoveQuestionAnswer(int questionAnswerId, int courseId)
        {
            try
            {
                var qa = context.ReadQuesAnswer(questionAnswerId);
                var course = context.ReadCourse(courseId);
                context.RemoveQAFromACourse(qa, course);
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }

            return RedirectToAction("Details", new { id = courseId });
        }

        [HttpGet]
        public IActionResult PrintPdfFormat(bool list = true, int courseId = 1)
        {
            PdfData data = new PdfData();

            if(list)
            {
                var courses = context.ReadAllCourses();
                var displayItems = new List<ItemsToDisplay>();

                var headers = new List<Tuple<string, int>>()
                {
                    new Tuple<string, int>("Id", 1),
                    new Tuple<string, int>("Name", 4),
                    new Tuple<string, int>("Semester", 3),
                    new Tuple<string, int>("Section", 2),
                    new Tuple<string, int>("Instructor", 4),
                    new Tuple<string, int>("Concentration", 5)
                };

                var rows = new List<List<string>>();
                foreach (var n in courses)
                {
                    var column = new List<string>();

                    column.Add(n.Id.ToString());
                    column.Add(n.Name);
                    column.Add(n.Semester.ToString());
                    column.Add(n.Section);
                    column.Add(n.Instructor.FullName);
                    column.Add(n.Concentration != null ? n.Concentration.Name : "");

                    rows.Add(column);
                }

                data = new PdfData
                {
                    DocumentTitle = "Courses",
                    DocumentName = "CoursesReport",
                    CreatedBy = User.Identity.Name ?? "Unknown",         // Ideally it'll be the user who is logged in
                    ReportName = "Course Title",
                    Table = new PdfTable() { Headers = headers, RowData = rows },
                    ReportDescription = $"Amount of Courses: {courses.Count()}\nAmount of Concentrations: {courses.Select(m => m.Semester).Distinct().Count()}"
                };
                var desc = $"Amount of Courses: {courses.Count()}\nAmount of Concentrations: {courses.Select(m => m.Semester).Distinct().Count()}";

                var parameters = new List<string>()
                {
                    "Id",
                    "Instructor"
                };

               // data = new PdfData((courses as IEnumerable<object>).Cast<object>().ToList(), parameters.ToArray(), "Courses", "Course", desc, User.Identity.Name ?? "Unknown");
            }



            var path = migraDocService.CreateMigraDocPdf(data);

            var stream = new FileStream(path, FileMode.Open);
            return File(stream, "application/pdf");
        }

        public ActionResult GetList()
        {
            var model = context.ReadAllCourses();
            return Json(new { data = model });
        }
    }
}