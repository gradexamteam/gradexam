﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Services;
using GradExam.ViewModels;
using GradExam.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace GradExam.Controllers
{
    [Authorize(Roles = "Admin,Director")]
    public class LearnOutcomeController : Controller
    {
        private IGradeExamRepository context;

        public LearnOutcomeController(IGradeExamRepository context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Delete(int id)
        {
            LearnOutcome Lout = context.ReadOutcome(id);
            if (Lout == null)
            {
                return RedirectToAction("Index");
            }
            return View(Lout);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            context.DeleteOutcome(id);
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            LearnOutcome learn = context.ReadOutcome(id);
            if (learn == null)
            {
                return RedirectToAction("Index");
            }
            return View(learn);
        }

        public IActionResult Update(int id)
        {
            // Get the list of Questions and Answers
            var listQA = context.ReadAllQuesAnswers().ToList();
            ViewBag.QAs = listQA;

            var model = context.ReadOutcome(id);

            var vm = new EditLearnOutcomeViewModel()
            {
                LearningOutcome = model,
                //QuestionId = model.QuestionAnswer.Id
            };

            return View(vm);
        }

        [HttpPost]
        public IActionResult Update(EditLearnOutcomeViewModel vm)
        {
            // Validate the model
            if (!ModelState.IsValid)
            {
                // Get the view model and return it
                return View("Update", vm);
            }

            //Add the question and answer to the learning outcome
            vm.LearningOutcome.QuestionAnswer.Add(context.ReadQuesAnswer(vm.QuestionId));
            context.UpdateOutcome(vm.LearningOutcome.Id, vm.LearningOutcome);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Add()
        {
            // Get the list of Questions and Answers
            var listQA = context.ReadAllQuesAnswers().ToList();
            ViewBag.QAs = listQA;

            var vm = new AddLearnOutcomeViewModel();
            return View(vm);
        }

        [HttpPost]
        public IActionResult Add(AddLearnOutcomeViewModel vm)
        {

            if (!ModelState.IsValid)
            {
                return View("Add", vm);
            }

            if (!ModelState.IsValid)
            {
                return View("Add", vm);
            }

            LearnOutcome Lo = new LearnOutcome
            {
                LearningOutcome = vm.LearningOutcome
            };

            context.CreateLearningOutcome(Lo);
            return RedirectToAction("Index");
        }

        public ActionResult GetList()
        {
            var model = context.ReadAllOutcomes();
            return Json(new { data = model });
        }


    }
}