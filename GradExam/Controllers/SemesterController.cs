﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GradExam.Controllers
{
    [Authorize(Roles = "Admin,Director")]
    public class SemesterController : Controller
    {
        private IGradeExamRepository context;

        public SemesterController(IGradeExamRepository context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Create(Semester semester)
        {
            if (ModelState.IsValid)
            {
                context.Create(semester);
                 return RedirectToAction("Index","Home");
            }
            return View();
        }

        public IActionResult Edit(int id)
        {
            var semester = context.ReadSemester(id);
            return View(semester);
        }
        
        [HttpPost]
        public IActionResult Edit(Semester semester)
        {
            if(ModelState.IsValid)
            {
                if(context.UpdateSemester(semester.SemesterId, semester))
                {
                    return View("Index");
                }
                else
                {
                    ModelState.AddModelError("","Matches the same year and season as another semester.");
                }
            }

            // Model State failed...re-route back to edit semester
            return View(semester);
        }

        public IActionResult Delete(int id)
        {
            var semester = context.ReadSemester(id);
            if (semester == null)
            {
                return RedirectToAction("Index");
            }
            return View(semester);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            if(context.DeleteSemester(id))
                return RedirectToAction("Index");

            ModelState.AddModelError("", "An error occured while deleting the semester. Please correct and try again.");
            return View(context.ReadSemester(id));
        }

        public ActionResult GetList()
        {
            var model = context.ReadAllSemesters();
            return Json(new { data = model });
        }
    }
}