﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using GradExam.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GradExam.Controllers
{
    [Authorize(Roles = "Admin,Director")]
    public class ExamController : Controller
    {
        IGradeExamRepository context;

        public ExamController(IGradeExamRepository context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var Exam = context.ReadAllExams();
            return View(Exam);
        }
        
        public IActionResult ViewExam(int id)
        {
            var exam = context.ReadExam(id);
            var vm = new ViewExamViewModel()
            {
                Exam = exam
            };

            var responses = new Dictionary<int, List<QuestionResponse>>();
            foreach(var item in exam.Student.StudentCourses)
            {
                responses.Add(item.Course.Id, context.ReadResponsesByCourse(item.Course.Id, exam.Id));
            }

            vm.Responses = responses;

            return View(vm);
        }

        [HttpPost]
        public IActionResult ViewExam(ViewExamViewModel vm)
        {
            var responses = vm.Responses;
            
            foreach(var item in responses)
            {
                var course = context.ReadResponsesByCourse(item.Key, vm.Exam.Id);

                foreach(var qr in course)
                {
                    foreach(var q in item.Value)
                    {
                        var r = context.ReadResponse(qr.Id);
                        r.Response = q.Response;
                        r.Rating = q.Rating;
                        context.SaveResponse(r);
                    }
                }

            }


            return RedirectToAction("ViewExam", new { id = vm.Exam.Id});
        }

        public IActionResult SelectExam()
        {
            var listAllStudents = context.ReadAllStudents().ToList();
            ViewBag.Students = listAllStudents;

            return View();
        }

        [HttpPost]
        public IActionResult SelectExam(int studentId)
        {
            var exam = context.CreateExam(studentId);

            return RedirectToAction("ViewExam", new { id = exam.Id });
        }
    }
}