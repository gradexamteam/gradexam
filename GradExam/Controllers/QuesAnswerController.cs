﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using GradExam.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;

namespace GradExam.Controllers
{
    [Authorize(Roles = "Instructor,Director,Admin")]
    public class QuesAnswerController : Controller
    {
        private IGradeExamRepository context;
        
        public QuesAnswerController(IGradeExamRepository context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var model = context.ReadAllQuesAnswers();
            return View(model);
        }

        public IActionResult Delete(int id)
        {
            QuesAnswer ques = context.ReadQuesAnswer(id);
            if (ques == null)
            {
                return RedirectToAction("Index");
            }
            return View(ques);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            context.deleteQuestion(id);
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            QuesAnswer ques = context.ReadQuesAnswer(id);
            if (ques == null)
            {
                return RedirectToAction("Index");
            }
            return View(ques);
        }

        [HttpGet]
        public IActionResult Add()
        {
            var vm = new AddQuesAnswerViewModel();

            // Get the list of courses for the dropdown
            vm.Courses = context.ReadAllCourses().ToList();
            vm.LearnOutcomes = context.ReadAllOutcomes().ToList();

            return View(vm);
        }
        
        [HttpPost]
        public IActionResult Add(AddQuesAnswerViewModel vm)
        {
            //Reinitialize the options for the select lists
            vm.Courses = context.ReadAllCourses().ToList();
            vm.LearnOutcomes = context.ReadAllOutcomes().ToList();

            //TODO: Fix the value cannot be null problem
            if (!ModelState.IsValid)
            {
                return View("Add", vm);
            }

            QuesAnswer Qa = new QuesAnswer
            {
                Question = vm.Question,
                Answer = vm.Answer,
                Course = context.ReadCourse(vm.CourseId),
                LearnOutcome = context.ReadOutcome(vm.LearnOutcomeId)
            };
            context.createQuesetion(Qa);
            return RedirectToAction("Index");
        }

        public IActionResult Update(int id)
        {
            var model = context.ReadQuesAnswer(id);

            var vm = new EditQuestionAnswerViewModel()
            {
                QuesAnswer = model,
                CourseId = model.Course.Id
            };

            if (model.LearnOutcome != null)
            {
                vm.LearnOutcomeId = model.LearnOutcome.Id;

            }

            // Get the list of courses for the dropdown
            vm.Courses = context.ReadAllCourses().ToList();
            vm.LearnOutcomes = context.ReadAllOutcomes().ToList();

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(vm);
        }

        [HttpPost]
        public IActionResult Update(EditQuestionAnswerViewModel vm)
        {
            vm.Courses = context.ReadAllCourses().ToList();
            vm.LearnOutcomes = context.ReadAllOutcomes().ToList();

            // Validate the model
            if (!ModelState.IsValid)
            {
                // Get the view model and return it
                return View("Update", vm);
            }


            // Add the qa
            try
            {
                // TODO: add a boolean to the update questions and answers
                vm.QuesAnswer.Course = context.ReadCourse(vm.CourseId);
                vm.QuesAnswer.LearnOutcome = context.ReadOutcome(vm.LearnOutcomeId);
                context.UpdateQuestion(vm.QuesAnswer.Id, vm.QuesAnswer);
            }
            catch
            {
                // For some reason the system could not create the Q&A. Redirect back to Update.
                ModelState.AddModelError(string.Empty, "Sorry, the system could not create the Q&A requested. Try again later.");
                return View("Update", vm);
            }

            return RedirectToAction("Index");
        }

        public ActionResult GetList()
        {
            var model = context.ReadAllQuesAnswers();
            return Json(new { data = model });
        }
      
    }
}