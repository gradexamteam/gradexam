﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GradExam.Models;
using GradExam.Services;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using GradExam.Services.PdfProvider.DataModel;
using GradExam.Services.PdfProvider;

namespace GradExam.Controllers
{
    [Authorize(Roles = "Admin,Director")]
    public class ReportController : Controller
    {
        private static List<Report> reportData;
        private static string reportName = "";

        IGradeExamRepository context;
        IMigraDocService _migraDoc;

        public ReportController(IGradeExamRepository context, IMigraDocService migraDoc) 
        {
            this.context = context;
            this._migraDoc = migraDoc;
        }

        public IActionResult Index() 
        {
            ViewBag.Semesters = context.GetDistinctSemesters();
            return View();
        }

        public ActionResult GetAggregateData(string time, int aggregate) {
            string agg = "";
            switch (aggregate) {
                case 1: agg = "Concentration";
                    break;
                case 2: agg = "Course";
                    break;
                case 3: agg = "Learning Outcome"; //return learning outcome
                    break;
                case 4: agg = "Question"; //return question
                    break;
            }
            reportName = agg;

            List<Report> data = context.ReadResponsesByAggregate(agg, time);
            reportData = data;
            return Json(data);
        }

        [HttpGet]
        public FileStreamResult PrintPDF()
        {
            var headers = new List<Tuple<string, int>>()
            {
                new Tuple<string, int>(reportName, 6),
                new Tuple<string, int>("Number of Students", 6),
                new Tuple<string, int>("Grade Average", 6)
            };

            var rows = new List<List<string>>();

            foreach (var n in reportData)
            {
                var column = new List<string>();

                if (n.Name != null)
                {
                    column.Add(n.Name.ToString());
                    int sum = 0;
                    foreach (var c in n.Courses)
                    {
                        sum += c.Item1.StudentCourses.Count();
                    }

                    column.Add(sum.ToString());

                    column.Add(n.ConcentrationAvg.ToString("0.##"));
                    rows.Add(column);
                }
                else
                {
                    foreach(var course in n.Courses)
                    {
                        column = new List<string>();
                        column.Add(course.Item1.Name);
                        column.Add(course.Item1.StudentCourses.Count().ToString());
                        column.Add(course.Item2.ToString("0.##"));
                        rows.Add(column);
                    }

                }

            }

            var data = new PdfData
            {
                DocumentTitle = "Report for " + reportName + "s",
                DocumentName = "AggregateReport",
                CreatedBy = User.Identity.Name ?? "Unknown",         // Ideally it'll be the user who is logged in
                ReportName = "Aggregate Report for " + reportName,
                Table = new PdfTable() { Headers = headers, RowData = rows },
                //ReportDescription = $"Amount of Courses: {courses.Count()}\nAmount of Concentrations: {courses.Select(m => m.Semester).Distinct().Count()}"
            };

            var path = _migraDoc.CreateMigraDocPdf(data);

            var stream = new FileStream(path, FileMode.Open);
            return File(stream, "application/pdf");
        }

    }
}