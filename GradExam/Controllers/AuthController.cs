﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using GradExam.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace GradExam.Controllers
{
    public class AuthController : Controller
    {
        private IGradeExamRepository context;
        private UserManager<User> userManager;
        private SignInManager<User> signInManager;
        private RoleManager<IdentityRole> roleManager;

        public AuthController(IGradeExamRepository context, UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<IdentityRole> roleManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region LogIn Actions

        public IActionResult LogIn()
        {
            var vm = new LogInViewModel();

            //TODO: uncomment when you wanna do ldap stuff
            //var ldap = new LDAPAuthenticationService();
            //ldap.GetConnection();

            return View(vm);
        }
        
        [HttpPost]
        public IActionResult LogIn(LogInViewModel vm)
        {
            var viewModel = new LogInViewModel
            {
                Username = vm.Username,
                Password = vm.Password
            };

            // If the user entered in invalid data
            if (!ModelState.IsValid)
                return View("Login", viewModel);

            // Try to log in the user
            var user = LogInUser(viewModel.Username, viewModel.Password).Result;

            // User sign in failed
            if (!user.Succeeded)
            {
                ModelState.AddModelError(string.Empty, "The username and/or password is incorrect. Please Try Again.");

                return View("Login", viewModel);
            }

            // If succeeded go to home index
            return RedirectToAction("Index", "Home", null);
  
        }
        #endregion

        #region LogOut Actions
        public async Task<IActionResult> LogOut()
        {
            await signInManager.SignOutAsync();
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            return RedirectToAction("Index", "Home", null);
        }
        #endregion

        #region Register Actions

        [HttpGet]
        public IActionResult Register()
        {
            var vm = new RegisterViewModel();

            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel vm)
        {

            // Validate the model
            if (!ModelState.IsValid)
            {
                // Get the view model and return it
                return View("Register", vm);
            }

            // Compare to see if the two passwords are the same
            if(vm.Password != vm.ConfirmPassword)
            {
                // Get the view model and return it with validation error: "Passwords do not match"
                return Content("Passwords do not match");
            }

            var userName = vm.Email.Split('@')[0];
            var userByEmailExists = userManager.Users.Any(user => user.Email == vm.Email);
            var userByUserNameExists = userManager.Users.Any(user => user.UserName == userName);

            // See if there is a user with the same username/email in the system
            if (userByUserNameExists || userByUserNameExists)
            {
                // Get the view model and return it with validation error: "Email already used for an existing user"
                ModelState.AddModelError(string.Empty, "User already exists in the system. Please use a different email address.");
                return View("Register", vm);
            }

            // Try to make user
            var newUser = new User
            {
                UserName = userName,
                Email = vm.Email,
                FirstName = vm.FirstName,
                LastName = vm.LastName
            };

            var result = await userManager.CreateAsync(newUser, vm.Password);


            // Handle any other exceptions
            if (result.Succeeded)
            {
                if(!await roleManager.RoleExistsAsync("Basic"))
                {
                    var role = new IdentityRole("Basic");
                    var res = await roleManager.CreateAsync(role);

                    if(res.Succeeded)
                    {
                        await userManager.AddToRoleAsync(newUser, "Basic");
                        await LogInUser(userName, vm.Password);
                    }

                }

                return RedirectToAction("Index", "Home", null);
            }

            // For some reason the system could not create the user. Redirect back to create.
            ModelState.AddModelError(string.Empty, "Sorry, the system could not create the user requested. Try again later.");
            return View("Register", vm);
        }
        #endregion

        #region Reset Password
        [HttpGet]
        public IActionResult ResetPassword()
        {
            var vm = new ResetPasswordViewModel();

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel vm)
        {
            var user = userManager.GetUserAsync(User).Result;
            var passwordCorrect = await userManager.CheckPasswordAsync(user, vm.OldPassword);
            var newPasswordSame = vm.ConfirmNewPassword == vm.NewPassword;

            if (!passwordCorrect)
            {
                ModelState.AddModelError("", "Password not correct for this user");
                return View("ResetPassword", vm);
            }

            if(!newPasswordSame)
            {
                ModelState.AddModelError("", "New passwords not the same");
                return View("ResetPassword", vm);
            }
                

            var change = await userManager.ChangePasswordAsync(user, vm.OldPassword, vm.NewPassword);
            
            if(change.Succeeded)
            {
                return RedirectToAction("Index", "Home", null);
            }

            ModelState.AddModelError("", "Failed for some reason, come back and try later");
            return View(vm);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Get the curent user
        /// </summary>
        /// <returns></returns>
        private Task<User> GetCurrentUserAsync() => userManager.GetUserAsync(HttpContext.User);
        
        /// <summary>
        /// Log in the user
        /// </summary>
        /// <param name="username">The username of the user</param>
        /// <param name="password">The password of the user</param>
        /// <returns></returns>
        private async Task<Microsoft.AspNetCore.Identity.SignInResult> LogInUser(string username, string password)
        {
            // Try to log in the user
            return await signInManager.PasswordSignInAsync(username, password, true, false);
        }
        #endregion

    }
}