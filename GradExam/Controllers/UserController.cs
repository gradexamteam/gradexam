﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using GradExam.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GradExam.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private IGradeExamRepository context;
        private UserManager<User> userManager;
        private RoleManager<IdentityRole> roleManager;

        public UserController(IGradeExamRepository context, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                context.CreateUser(user);
                return RedirectToAction("Index");
            }
            return View(user);
        }

        public IActionResult Edit(string id)
        {
            EditUserViewModel vm = new EditUserViewModel();
            vm.User = context.Read(id);
            vm.Roles = new List<SelectListItem>();
            vm.UserCurrentRoles = context.GetRolesOfUser(id);

            var roles = context.GetListOfRoles();

            foreach(var i in roles)
            {
                if(!vm.UserCurrentRoles.Contains(i))
                    vm.Roles.Add(new SelectListItem(i.Name, i.Name));
            }

            return View(vm);
        }

        [HttpPost]
        public IActionResult Edit(EditUserViewModel vm)
        {

            if(!ModelState.IsValid)
            {
                return RedirectToAction("Edit", vm.User.Id);
            }

            context.Update(vm.User.Id, vm.User);

            return RedirectToAction("Index");
        }

        public ActionResult GetList()
        {
            var model = context.ReadAllUsers();
            return Json(new { data = model });
        }

        #region UserRoleManagement
        public async Task<IActionResult> AddRole(string roleName, string userId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Edit", new { id = userId });
                }

                var user = context.Read(userId);
                var isExistRole = await this.roleManager.RoleExistsAsync(roleName);

                if (isExistRole)
                {
                    await userManager.AddToRoleAsync(user, roleName);
                }

            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }

            return RedirectToAction("Edit", new { id = userId });
        }
        public async Task<IActionResult> RemoveRole(string roleId, string userId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Edit", new { id = userId });
                }

                var user = context.Read(userId);
                var roleName = context.GetRole(roleId).Name;
                var isExistRole = await this.roleManager.RoleExistsAsync(roleName);

                if (isExistRole)
                {
                    await userManager.RemoveFromRoleAsync(user, roleName);
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }

            return RedirectToAction("Edit", new { id = userId });
        }


        #endregion

    }
}