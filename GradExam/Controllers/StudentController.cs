﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GradExam.Controllers
{
    [Authorize(Roles = "Admin,Director")]
    public class StudentController : Controller
    {
        private IGradeExamRepository context;

        public StudentController(IGradeExamRepository context)
        {
            this.context = context;
        }

        public IActionResult Delete(int id)
        {
            var student = context.ReadStudent(id);
            if (student == null)
            {
                return RedirectToAction("Index");
            }
            return View(student);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            context.DeleteStudent(id);
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            var student = context.ReadStudent(id);
            if (student == null)
            {
                return RedirectToAction("Index");
            }
            List<Tuple<string, double>> averages = context.GetAverageByClass(id);
            Tuple<Student, List<Tuple<string,double>>> studentDetails
                = new Tuple<Student, List<Tuple<string, double>>>(student, averages);
            return View(studentDetails);
        }

        

        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Add(Student student)
        {

            if (ModelState.IsValid)
            {
                context.AddStudent(student);
                return RedirectToAction("Index");
            }
            return View(student);

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Edit(int id)
        {
            var student = context.ReadStudent(id);
            if (student == null)
            {
                return RedirectToAction("Index");
            }
            return View(student);
        }

        [HttpPost]
        public IActionResult Edit(Student student)
        {
            if (ModelState.IsValid)
            {
                context.UpdateStudent(student.Id, student);
                return RedirectToAction("Index");
            }
            return View(student);
        }

        public ActionResult GetList()
        {
            var model = context.ReadAllStudents();
            return Json(new { data = model });
        }
    }
}