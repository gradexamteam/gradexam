﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GradExam.Models;
using GradExam.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using GradExam.ViewModels;
using GradExam.Services.PdfProvider.DataModel;
using GradExam.Services.PdfProvider;
using System.IO;

namespace GradExam.Controllers
{
    public class HomeController : Controller
    {
        private IGradeExamRepository context;
        private readonly IPdfSharpService _pdfService;
        private readonly IMigraDocService _migraDocService;

        public HomeController(IGradeExamRepository context, IPdfSharpService pdfService, IMigraDocService migraDocService)
        {
            this.context = context;
            this._pdfService = pdfService;
            this._migraDocService = migraDocService;
        }

        public IActionResult Index()
        {
            var vm = new HomeIndexViewModel
            {
                Users = context.ReadAllUsers().ToList()
            };

            return View(vm);
        }


        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Making sure the Roles work
        [Authorize(Roles = "Admin")]
        public IActionResult Admin()
        {
            return Content("Admin works");
        }

        [Authorize(Roles = "Director")]
        public IActionResult Director()
        {
            return Content("Director works");
        }

        [Authorize(Roles = "Instructor")]
        public IActionResult Instructor()
        {
            return Content("Instructor works");
        }

        [Authorize(Roles = "Basic")]
        public IActionResult Basic()
        {
            return Content("It works");
        }
        #endregion

        #region PDF Example Methods
        //[HttpGet]
        //public FileStreamResult CreatePdf()
        //{
        //    var courses = context.ReadAllCourses();
        //    var displayItems = new List<ItemsToDisplay>();

        //    var headers = new List<Tuple<string, int>>()
        //    {
        //        new Tuple<string, int>("Id", 25),
        //        new Tuple<string, int>("Name", 200),
        //        new Tuple<string, int>("Section", 100),
        //        new Tuple<string, int>("Semester", 100),
        //        new Tuple<string, int>("Instructor", 200)
        //    };

        //    var rows = new List<List<string>>();
        //    foreach(var n in courses)
        //    {
        //        var column = new List<string>();

        //        column.Add(n.Id.ToString());
        //        column.Add(n.Name);
        //        column.Add(n.Section);
        //        column.Add(n.Semester);
        //        column.Add(n.Instructor.FullName);

        //        rows.Add(column);
        //    }

        //    var data = new PdfData
        //    {
        //        DocumentTitle = "Courses",
        //        DocumentName = "CoursesReport",
        //        CreatedBy = User.Identity.Name ?? "Unknown",         // Ideally it'll be the user who is logged in
        //        ReportName = "Course",
        //        Table = new PdfTable() { Headers = headers, RowData = rows },
        //        ReportDescription = $"Amount of Courses: {courses.Count()}\nAmount of Concentrations: {courses.Select(m => m.Semester).Count()}"
        //    };



        //    var path = _pdfService.CreatePdf(data);

        //    var stream = new FileStream(path, FileMode.Open);
        //    return File(stream, "application/pdf");
        //}

        //[HttpGet]
        //public FileStreamResult CreateMigraDocPdf(PdfData pdfData)
        //{
        //    var courses = context.ReadAllCourses();
        //    var displayItems = new List<ItemsToDisplay>();

        //    var headers = new List<Tuple<string, int>>()
        //    {
        //        new Tuple<string, int>("Id", 2),
        //        new Tuple<string, int>("Name", 5),
        //        new Tuple<string, int>("Section", 2),
        //        new Tuple<string, int>("Semester", 3),
        //        new Tuple<string, int>("Instructor", 5)
        //    };

        //    var rows = new List<List<string>>();
        //    foreach (var n in courses)
        //    {
        //        var column = new List<string>();

        //        column.Add(n.Id.ToString());
        //        column.Add(n.Name);
        //        column.Add(n.Section);
        //        column.Add(n.Semester);
        //        column.Add(n.Instructor.FullName);

        //        rows.Add(column);
        //    }

        //    var data = new PdfData
        //    {
        //        DocumentTitle = "Courses",
        //        DocumentName = "CoursesReport",
        //        CreatedBy = User.Identity.Name ?? "Unknown",         // Ideally it'll be the user who is logged in
        //        ReportName = "Course",
        //        Table = new PdfTable() { Headers = headers, RowData = rows },
        //        ReportDescription = $"Amount of Courses: {courses.Count()}\nAmount of Concentrations: {courses.Select(m => m.Semester).Distinct().Count()}"
        //    };

        //    var path = _migraDocService.CreateMigraDocPdf(pdfData);

        //    var stream = new FileStream(path, FileMode.Open);
        //    return File(stream, "application/pdf");
        //}
        #endregion
    }
}
