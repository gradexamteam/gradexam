﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using GradExam.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GradExam.Controllers
{
    [Authorize(Roles ="Admin,Director")]
    public class ConcentrationController : Controller
    {
        private IGradeExamRepository _context;

        public ConcentrationController(IGradeExamRepository context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var concentrations = _context.ReadAllConcentrations();
            return View(concentrations);
        }

        public IActionResult Edit(int id, bool read = false)
        {
            var concentration = _context.ReadConcentration(id);

            var vm = new EditConcentrationViewModel
            {
                ReadOnly = read,
                Id = concentration.Id,
                Title = concentration.Name
            };

            return View(vm);
        }

        [HttpPost]
        public IActionResult Edit(int id, EditConcentrationViewModel vm)
        {
            //Make sure the model state is valid
            if (!ModelState.IsValid)
            {
                return View("Edit", vm);
            }

            Concentration concentration;

            // If we are adding a concentration
            if (id == -1)
            {
                concentration = new Concentration
                {
                    Name = vm.Title
                };

                _context.UpdateConcentration(-1, concentration);
            }
            else //Editing a concentration
            {
                concentration = _context.ReadConcentration(id);

                concentration.Name = vm.Title;

                _context.UpdateConcentration(id, concentration);
            }

            return RedirectToAction("Index");
        }

        public IActionResult Add()
        {
            //Create view model with -1 as id to signify we are adding a concentration
            var vm = new EditConcentrationViewModel();
            vm.Id = -1;

            return View("Edit",vm);
        }

        public IActionResult Delete(int id)
        {
            var con = _context.ReadConcentration(id);
            if (con == null)
            {
                return RedirectToAction("Index");
            }
            return View(con);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            _context.DeleteConcentration(id);
            return RedirectToAction("Index");
        }

		public ActionResult GetList()
		{
			var model = _context.ReadAllConcentrations();
			return Json(new { data = model });
		}
	}
}